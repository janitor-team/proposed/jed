% defaults.sl
%
% Evaluate all *.sl files in /etc/jed.d/
%
% At startup, site.sl looks for the existence of "defaults.sl" and loads
% it.  This file IS NOT distributed with JED (upstream) but added by the
% Debian jed-common package.
%
%  Copyright © 2005 The Debian Jed Maintainers Group
%  Released under the terms of the GNU General Public License (ver. 2 or later)

% dummy function, enables skipping the startup procecure from the command line
% with `jed --skip-debian-startup`
define skip_debian_startup() {}

% Evaluate all *.sl files in the jed configuration directory "/etc/jed.d/"
% in alphabetical order
define debian_startup()
{
    variable file, dir = listdir("/etc/jed.d/");
    foreach file ( dir[array_sort(dir)] )
     {
        if (path_extname(file) == ".sl")
          () = evalfile(strcat("/etc/jed.d/", file));
     }
}

if ( andelse {BATCH != 2}
     % skip startup scripts if jed is started as `jed-script`
     {wherefirst(__argv == "--skip-debian-startup") == NULL}
     % skip startup scripts if jed is started with --skip-debian-startup
   )
     debian_startup();
